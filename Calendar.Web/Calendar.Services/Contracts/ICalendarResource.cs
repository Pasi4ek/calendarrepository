﻿using System.ServiceModel;
using Calendar.Services.Dto;

namespace Calendar.Services.Contracts
{
    /// <summary>
    /// Interface for work calendar resources
    /// </summary>
    [ServiceContract]
    public interface ICalendarResource
    {
        /// <summary>
        /// Create new calendar resource
        /// </summary>
        /// <param name="calendarResource">CalendarResourceDto</param>
        /// <returns>Returns the result of executing the command</returns>
        [OperationContract]
        bool CreateCalendarResource(CalendarResourceDto calendarResource);

        /// <summary>
        /// Read calendar resources
        /// </summary>
        /// <returns>Return array calendar resource</returns>
        [OperationContract]
        CalendarResourceDto[] ReadCalendarResource();

        /// <summary>
        /// Update calendar resource
        /// </summary>
        /// <param name="ID">Int</param>
        /// <param name="calendarResource">CalendarResourceDto</param>
        /// <returns>Returns the result of executing the command</returns>
        [OperationContract]
        bool UpdateCalendarResource(int ID, CalendarResourceDto calendarResource);

        /// <summary>
        /// Delete calendar resource
        /// </summary>
        /// <param name="ID">Int</param>
        /// <returns>Returns the result of executing the command</returns>
        [OperationContract]
        bool DeleteCalendarResource(int ID);
    }
}
