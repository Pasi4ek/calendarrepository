﻿using System.ServiceModel;
using Calendar.Services.Dto;

namespace Calendar.Services.Contracts
{
    /// <summary>
    /// Interface for work calendars
    /// </summary>
    [ServiceContract]
    public interface ICalendar
    {
        /// <summary>
        /// Create new calendar
        /// </summary>
        /// <param name="calendar">Calendar</param>
        /// <returns>Returns the result of executing the command</returns>
        [OperationContract]
        bool CreateCalendar(CalendarDto calendar);

        /// <summary>
        /// Read calendars
        /// </summary>
        /// <returns>Returns array calendars</returns>
        [OperationContract]
        CalendarDto[] ReadCalendar();

        /// <summary>
        /// Update calendar
        /// </summary>
        /// <param name="ID">Int</param>
        /// <param name="calendar">Calendar</param>
        /// <returns>Returns the result of executing the command</returns>
        [OperationContract]
        bool UpdateCalendar(int ID, CalendarDto calendar);

        /// <summary>
        /// Delete calendar
        /// </summary>
        /// <param name="ID">Int</param>
        /// <returns>Returns the result of executing the command</returns>
        [OperationContract]
        bool DeleteCalendar(int ID);
    }
}
