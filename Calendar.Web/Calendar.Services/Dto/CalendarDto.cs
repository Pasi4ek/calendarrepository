﻿using System;
using System.Runtime.Serialization;

namespace Calendar.Services.Dto
{
    /// <summary>
    /// Class CalendarDto
    /// </summary>
    [DataContract]
    public class CalendarDto
    {
        /// <summary>
        /// Gets or sets Caption
        /// </summary>
        [DataMember]
        public string Caption { get; set; }

        /// <summary>
        /// Gets or sets Start
        /// </summary>
        [DataMember]
        public DateTime Start { get; set; }

        /// <summary>
        /// Gets or sets Message
        /// </summary>
        [DataMember]
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets Location
        /// </summary>
        [DataMember]
        public string Location { get; set; }

        /// <summary>
        /// Gets or sets Fuel
        /// </summary>
        [DataMember]
        public double Fuel { get; set; }

        /// <summary>
        /// Gets or sets Distance
        /// </summary>
        [DataMember]
        public double Distance { get; set; }

        /// <summary>
        /// Gets or sets Client
        /// </summary>
        [DataMember]
        public string Client { get; set; }

        /// <summary>
        /// Gets or sets Quality
        /// </summary>
        [DataMember]
        public double Quality { get; set; }

        /// <summary>
        /// Gets or sets Approved
        /// </summary>
        [DataMember]
        public string Approved { get; set; }

        /// <summary>
        /// Gets or sets Name
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets Longitube
        /// </summary>
        [DataMember]
        public string Longitube { get; set; }

        /// <summary>
        /// Gets or sets Latitude
        /// </summary>
        [DataMember]
        public string Latitude { get; set; }

        /// <summary>
        /// Gets or sets EbentDuraction
        /// </summary>
        [DataMember]
        public double EventDuraction { get; set; }

        /// <summary>
        /// Gets or sets Finish
        /// </summary>
        [DataMember]
        public DateTime Finish { get; set; }

        /// <summary>
        /// Gets or sets EventType
        /// </summary>
        [DataMember]
        public int EventType { get; set; }

        /// <summary>
        /// Gets or sets Options
        /// </summary>
        [DataMember]
        public int Options { get; set; }

        /// <summary>
        /// Gets or sets ResourceID 
        /// </summary>
        [DataMember]
        public int ResourceID { get; set; }

        /// <summary>
        /// Gets or sets ID
        /// </summary>
        [DataMember]
        public long ID { get; set; }

        /// <summary>
        /// Gets or sets LabelColor
        /// </summary>
        [DataMember]
        public int LabelColor { get; set; }

        /// <summary>
        /// Gets or sets State
        /// </summary>
        [DataMember]
        public int State { get; set; }

        /// <summary>
        /// Gets or sets TaskID
        /// </summary>
        [DataMember]
        public int TaskID { get; set; }

        /// <summary>
        /// Gets or sets ResultMessage
        /// </summary>
        [DataMember]
        public string ResultMessage { get; set; }

        /// <summary>
        /// Gets or sets ClientID
        /// </summary>
        [DataMember]
        public long ClientID { get; set; }

        /// <summary>
        /// Gets or sets ModifiedUTCTimeStamp
        /// </summary>
        [DataMember]
        public DateTime ModifiedUTCTimeStamp { get; set; }

        /// <summary>
        /// Gets or sets MaxDiteTime
        /// </summary>
        [DataMember]
        public DateTime MaxDiteTime2 { get; set; }

        /// <summary>
        /// Gets or sets IsDeleted
        /// </summary>
        [DataMember]
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets ManualChecked
        /// </summary>
        [DataMember]
        public bool ManualChecked { get; set; }

        /// <summary>
        /// Gets or sets ParentID
        /// </summary>
        [DataMember]
        public int ParentID { get; set; }

        /// <summary>
        /// Gets or sets RecurrenceIndex
        /// </summary>
        [DataMember]
        public int RecurrenceIndex { get; set; }

        /// <summary>
        /// Gets or sest RecurrenceInfo
        /// </summary>
        [DataMember]
        public byte[] RecurrenceInfo { get; set; }
    }
}
