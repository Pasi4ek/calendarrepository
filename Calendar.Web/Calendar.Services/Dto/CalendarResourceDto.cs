﻿using System;
using System.Runtime.Serialization;
using System.Drawing;

namespace Calendar.Services.Dto
{
    /// <summary>
    /// Class CalendarResourceDto
    /// </summary>
    [DataContract]
    public class CalendarResourceDto
    {
        /// <summary>
        /// Gets or sets ID
        /// </summary>
        [DataMember]
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets ResourceID
        /// </summary>
        [DataMember]
        public int ResourceID { get; set; }

        /// <summary>
        /// Gets or sets ResourceName
        /// </summary>
        [DataMember]
        public string ResourceName { get; set; }

        /// <summary>
        /// Gets or sets Color
        /// </summary>
        [DataMember]
        public int Color { get; set; }

        /// <summary>
        /// Gets or sets Image
        /// </summary>
        [DataMember]
        public Image Image { get; set; }

        /// <summary>
        /// Gets or sets CustomField1
        /// </summary>
        [DataMember]
        public string CustomField1 { get; set; }

        /// <summary>
        /// Gets or sets ModifiedUTCTimeStamp
        /// </summary>
        [DataMember]
        public DateTime ModifiedUTCTimeStamp { get; set; }

        /// <summary>
        /// Gets or sets MaxDateTime2
        /// </summary>
        [DataMember]
        public DateTime MaxDateTime2 { get; set; }

        /// <summary>
        /// Gets or sets IsDeleted
        /// </summary>
        [DataMember]
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets EmployeeID
        /// </summary>
        [DataMember]
        public long EmployeeID { get; set; }
    }
}
