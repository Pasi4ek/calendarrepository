﻿using System.Collections.Generic;
using Calendar.Data.Repositories;
using Calendar.Entities.MyWork;

namespace Calendar.Core.Providers
{
    public class CalendarProvider : ICalendarProvider
    {

        private ICalendarRepository _calendarRepository;

        public CalendarProvider(ICalendarRepository calendarRepository)
        {
            _calendarRepository = calendarRepository;
        }

        public bool CreateCalendar(CalendarEntity calendar)
        {
            return _calendarRepository.CreateCalendar(calendar);
        }

        public bool DeleteCalendar(int ID)
        {
            return _calendarRepository.DeleteCalendar(ID);
        }

        public IReadOnlyList<CalendarEntity> ReadCalendar()
        {
            return _calendarRepository.ReadCalendar();
        }

        public bool UpdateCalendar(int ID, CalendarEntity calendar)
        {
            return _calendarRepository.UpdateCalendar(ID, calendar);
        }
    }
}
