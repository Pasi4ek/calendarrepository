﻿using Calendar.Core.Providers;
using StructureMap.Configuration.DSL;

namespace Calendar.Core.Container
{
    public class CoreRegistry : Registry
    {

        public CoreRegistry()
        {
            For<ICalendarProvider>().Use<CalendarProvider>();
        }

    }
}
