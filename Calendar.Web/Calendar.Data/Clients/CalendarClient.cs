﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Calendar.Entities.MyWork;

namespace Calendar.Data.Clients
{
    public class CalendarClient : ICalendarClient
    {
        private static ILog log = LogManager.GetLogger("LOGGER");

        public bool CreateCalendar(CalendarEntity calendar)
        {
            throw new NotImplementedException();
        }

        public bool DeleteCalendar(int ID)
        {
            throw new NotImplementedException();
        }

        public IReadOnlyList<CalendarEntity> ReadCalendar()
        {
            
            var calendarList = new List<CalendarEntity>();

            using (var client = new CalendarService.CalendarClient())
            {

                try
                {

                    client.Open();

                    var calendarDto = client.ReadCalendar();

                    if (calendarDto != null)
                    {
                        foreach (var calendars in calendarDto)
                        {
                            var calendar = new CalendarEntity()
                            {
                                ID = calendars.ID,
                                Caption = calendars.Caption,
                                ResourceID = calendars.ResourceID,
                                Approved = calendars.Approved,
                                Client = calendars.Client,
                                ClientID = calendars.ClientID,
                                Distance = calendars.Distance,
                                EventDuraction = calendars.EventDuraction,
                                EventType = calendars.EventType,
                                Finish = calendars.Finish,
                                Fuel = calendars.Fuel,
                                IsDeleted  = calendars.IsDeleted,
                                LabelColor = calendars.LabelColor,
                                Latitude = calendars.Latitude,
                                Location = calendars.Location,
                                Longitube = calendars.Longitube,
                                ManualChecked = calendars.ManualChecked,
                                MaxDiteTime2 = calendars.MaxDiteTime2,
                                Message = calendars.Message,
                                ModifiedUTCTimeStamp = calendars.ModifiedUTCTimeStamp,
                                Name = calendars.Name,
                                Options = calendars.Options,
                                ParentID = calendars.ParentID,
                                Quality = calendars.Quality,
                                RecurrenceIndex = calendars.RecurrenceIndex,
                                RecurrenceInfo = calendars.RecurrenceInfo,
                                ResultMessage = calendars.ResultMessage,
                                Start = calendars.Start,
                                State = calendars.State,
                                TaskID = calendars.TaskID
                            };
                            calendarList.Add(calendar);
                        }
                    }

                    client.Close();
                }
                catch (Exception e)
                {
                    log.Error(e.Message);
                }
            }

            return calendarList;

        }

        public bool UpdateCalendar(int ID, CalendarEntity calendar)
        {
            throw new NotImplementedException();
        }
    }
}
