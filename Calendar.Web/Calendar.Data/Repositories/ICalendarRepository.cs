﻿using System.Collections.Generic;
using Calendar.Entities.MyWork;

namespace Calendar.Data.Repositories
{
    public interface ICalendarRepository
    {
        /// <summary>
        /// Create new calendar
        /// </summary>
        /// <param name="calendar">CalendarEntity</param>
        /// <returns>Returns the result of executing the command</returns>
        bool CreateCalendar(CalendarEntity calendar);

        /// <summary>
        /// Read calendars
        /// </summary>
        /// <returns>Returns array calendars</returns>
        IReadOnlyList<CalendarEntity> ReadCalendar();

        /// <summary>
        /// Update calendar
        /// </summary>
        /// <param name="ID">Int</param>
        /// <param name="calendar">CalendarEntity</param>
        /// <returns>Returns the result of executing the command</returns>
        bool UpdateCalendar(int ID, CalendarEntity calendar);

        /// <summary>
        /// Delete calendar
        /// </summary>
        /// <param name="ID">Int</param>
        /// <returns>Returns the result of executing the command</returns>
        bool DeleteCalendar(int ID);
    }
}
