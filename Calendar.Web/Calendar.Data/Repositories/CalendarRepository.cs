﻿using System.Collections.Generic;
using Calendar.Data.Clients;
using Calendar.Entities.MyWork;

namespace Calendar.Data.Repositories
{
    public class CalendarRepository : ICalendarRepository
    {
        private ICalendarClient _сalendarClient;

        public CalendarRepository(ICalendarClient calendarClient)
        {
            if (calendarClient != null)
            {
                _сalendarClient = calendarClient;
            }
        }

        public bool CreateCalendar(CalendarEntity calendar)
        {
            return _сalendarClient.CreateCalendar(calendar);
        }

        public bool DeleteCalendar(int ID)
        {
            return _сalendarClient.DeleteCalendar(ID);
        }

        public IReadOnlyList<CalendarEntity> ReadCalendar()
        {
            return _сalendarClient.ReadCalendar();
        }

        public bool UpdateCalendar(int ID, CalendarEntity calendar)
        {
            return _сalendarClient.UpdateCalendar(ID, calendar);
        }
    }
}
