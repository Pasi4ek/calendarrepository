﻿using Calendar.Data.Clients;
using Calendar.Data.Repositories;
using StructureMap.Configuration.DSL;

namespace Calendar.Data.Container
{
    public class DataRegistry : Registry
    {
        public DataRegistry()
        {
            For<ICalendarClient>().Use<CalendarClient>();
            For<ICalendarRepository>().Use<CalendarRepository>();
        }
    }
}
