﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

using Calendar.Services.Contracts;
using Calendar.Services.Dto;
using log4net;

namespace Calendar.WCFServices.MyWork
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CalendarWCF" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select CalendarWCF.svc or CalendarWCF.svc.cs at the Solution Explorer and start debugging.
    public class CalendarWCF : ICalendar
    {

        #region CONSTS SQL 

        private const string COLUMN_ID = "ID";
        private const string COLUMN_CAPTION = "Caption";
        private const string COLUMN_START = "Start";
        private const string COLUMN_MESSAGE = "Message";
        private const string COLUMN_LOCATION = "Location";
        private const string COLUMN_FUEL = "Fuel";
        private const string COLUMN_DISTANCE = "Distance";
        private const string COLUMN_CLIENT = "Client";
        private const string COLUMN_QUALITY = "Quality";
        private const string COLUMN_APPROVED = "Approved";
        private const string COLUMN_NAME = "Name";
        private const string COLUMN_LONGITUBE = "Longitude";
        private const string COLUMN_LATITUDE = "Latitude";
        private const string COLUMN_EVENT_DURACTION = "EventDuration";
        private const string COLUMN_FINISH = "Finish";
        private const string COLUMN_EVENT_TYPE = "EventType";
        private const string COLUMN_OPTIONS = "Options";
        private const string COLUMN_RESOURCE_ID = "ResourceID";
        private const string COLUMN_LABEL_COLOR = "LabelColor";
        private const string COLUMN_STATE = "State";
        private const string COLUMN_TASK_ID = "TaskID";
        private const string COLUMN_RESULT_MESSAGE = "ResultMessage";
        private const string COLUMN_CLIENT_ID = "ClientID";
        private const string COLUMN_MODIFIED_UTC_TIME_STAMP = "ModifiedUTCTimeStamp";
        private const string COLUMN_MAX_DATE_TIME2 = "MaxDateTime2";
        private const string COLUMN_IS_DELETED = "IsDeleted";
        private const string COLUMN_MANUAL_CHECKED = "ManualChecked";
        private const string COLUMN_PARENT_ID = "ParentID";
        private const string COLUMN_RECURRENCE_INDEX = "RecurrenceIndex";
        private const string COLUMN_RECURRENCE_INFO = "RecurrenceInfo";

        private const string SP_READ_CALENDAR = "MyWork_ReadCalendar";
        
        #endregion

        private string ConnectionString = ConfigurationManager.ConnectionStrings["TestDB"].ConnectionString;

        private ILog log = LogManager.GetLogger("LOGGER");

        public bool CreateCalendar(CalendarDto calendar)
        {
            throw new NotImplementedException();
        }

        public bool DeleteCalendar(int ID)
        {
            throw new NotImplementedException();
        }

        public CalendarDto[] ReadCalendar()
        {
            var calendarList = new List<CalendarDto>();

            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                using (var sqlCommand = new SqlCommand(SP_READ_CALENDAR, sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        sqlConnection.Open();

                        using (SqlDataReader reader = sqlCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var calendar = new CalendarDto();


                                calendar.Caption = reader[COLUMN_CAPTION] as string;
                                calendar.Start = reader[COLUMN_START] as DateTime? ?? default(DateTime);
                                calendar.Message = reader[COLUMN_MESSAGE] as string;
                                calendar.Location = reader[COLUMN_LOCATION] as string;
                                calendar.Fuel = reader[COLUMN_FUEL] as double? ?? default(double);
                                calendar.Distance = reader[COLUMN_DISTANCE] as double? ?? default(double);
                                calendar.Client = reader[COLUMN_CLIENT] as string;
                                calendar.Quality = reader[COLUMN_QUALITY] as double? ?? default(double);
                                calendar.Approved = reader[COLUMN_APPROVED] as string;
                                calendar.Name = reader[COLUMN_NAME] as string;
                                calendar.Longitube = reader[COLUMN_LONGITUBE] as string;
                                calendar.Latitude = reader[COLUMN_LATITUDE] as string;
                                calendar.EventDuraction = reader[COLUMN_EVENT_DURACTION] as double? ?? default(double);
                                calendar.Finish = reader[COLUMN_FINISH] as DateTime? ?? default(DateTime);
                                calendar.EventType = reader[COLUMN_EVENT_TYPE] as int? ?? default(int);
                                calendar.Options = reader[COLUMN_OPTIONS] as int? ?? default(int);
                                calendar.ResourceID = reader[COLUMN_RESOURCE_ID] as int? ?? default(int);
                                calendar.ID = reader[COLUMN_ID] as long? ?? default(long);
                                calendar.LabelColor = reader[COLUMN_LABEL_COLOR] as int? ?? default(int);
                                calendar.State = reader[COLUMN_STATE] as int? ?? default(int);
                                calendar.TaskID = reader[COLUMN_TASK_ID] as int? ?? default(int);
                                calendar.ResultMessage = reader[COLUMN_RESULT_MESSAGE] as string;
                                calendar.ClientID = reader[COLUMN_CLIENT_ID] as long? ?? default(long);
                                calendar.ModifiedUTCTimeStamp = reader[COLUMN_MODIFIED_UTC_TIME_STAMP] as DateTime? ?? default(DateTime);
                                calendar.MaxDiteTime2 = reader[COLUMN_MAX_DATE_TIME2] as DateTime? ?? default(DateTime);
                                calendar.IsDeleted = reader[COLUMN_IS_DELETED] as bool? ?? default(bool);
                                calendar.ManualChecked = reader[COLUMN_MANUAL_CHECKED] as bool? ?? default(bool);
                                calendar.ParentID = reader[COLUMN_PARENT_ID] as int? ?? default(int);
                                calendar.RecurrenceIndex = reader[COLUMN_RECURRENCE_INDEX] as int? ?? default(int);
                                if (!reader.IsDBNull(27))
                                    calendar.RecurrenceInfo = (byte[])reader[COLUMN_RECURRENCE_INFO];
                                else calendar.RecurrenceInfo = new byte[1];
                                calendarList.Add(calendar);
                            }
                        }

                        sqlConnection.Close();
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex.Message);
                        sqlConnection.Close();
                    }
                }
            }

            return calendarList.ToArray();
        }

        public bool UpdateCalendar(int ID, CalendarDto calendar)
        {
            throw new NotImplementedException();
        }
    }
}
