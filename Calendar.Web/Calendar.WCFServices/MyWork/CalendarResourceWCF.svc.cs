﻿using Calendar.Services.Contracts;
using Calendar.Services.Dto;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Calendar.WCFServices.MyWork
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CalendarResourceWCF" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select CalendarResourceWCF.svc or CalendarResourceWCF.svc.cs at the Solution Explorer and start debugging.
    public class CalendarResourceWCF : ICalendarResource
    {

        #region CONST SQL

        private const string COLUMN_ID = "ID";
        private const string COLUMN_RESOURCE_ID = "ResourceID";
        private const string COLUMN_RESOURCE_NAME = "ResourceName";
        private const string COLUMN_COLOR = "Color";
        private const string COLUMN_IMAGE = "Image";
        private const string COLUMN_CUSTOM_FIELD1 = "CustomField1";
        private const string COLUMN_MODIFIED_UTC_TIME_STAMP = "ModifiedUTCTimeStamp";
        private const string COLUMN_MAX_DATE_TIME2 = "MaxDateTime2";
        private const string COLUMN_IS_DELETED = "IsDeleted";
        private const string COLUMN_EMPLOYEE_ID = "EmployeeID";

        #endregion

        private const string SP_READ_CATEGORY_RESOURCE = "MyWork_ReadCalendarResources";

        private string ConnectionString = ConfigurationManager.ConnectionStrings["TestDB"].ConnectionString;

        private ILog log = LogManager.GetLogger("LOGGER");


        public bool CreateCalendarResource(CalendarResourceDto calendarResource)
        {
            throw new NotImplementedException();
        }

        public bool DeleteCalendarResource(int ID)
        {
            throw new NotImplementedException();
        }

        public CalendarResourceDto[] ReadCalendarResource()
        {
            var calendarResourceList = new List<CalendarResourceDto>();

            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                using (var sqlCommand = new SqlCommand(SP_READ_CATEGORY_RESOURCE, sqlConnection))
                {
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        sqlConnection.Open();

                        using (SqlDataReader reader = sqlCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var calendarResource = new CalendarResourceDto();

                                calendarResource.ID = reader[COLUMN_ID] as int? ?? default(int);
                                calendarResource.ResourceID = reader[COLUMN_RESOURCE_ID] as int? ?? default(int);
                                calendarResource.Color = reader[COLUMN_COLOR] as int? ?? default(int);
                                calendarResource.CustomField1 = reader[COLUMN_RESOURCE_NAME] as string;
                                calendarResource.CustomField1 = reader[COLUMN_CUSTOM_FIELD1] as string;
                                calendarResource.ModifiedUTCTimeStamp = reader[COLUMN_MODIFIED_UTC_TIME_STAMP] as DateTime? ?? default(DateTime);
                                calendarResource.MaxDateTime2 = reader[COLUMN_MAX_DATE_TIME2] as DateTime? ?? default(DateTime);
                                calendarResource.IsDeleted = reader[COLUMN_IS_DELETED] as bool? ?? default(bool);
                                calendarResource.EmployeeID = reader[COLUMN_EMPLOYEE_ID] as long? ?? default(long);
                                
                               /* if (!reader.IsDBNull(5))
                                    calendarResource.Image = (byte[])reader[COLUMN_IMAGE];
                                else calendarResource.RecurrenceInfo = new byte[1];*/
                                calendarResourceList.Add(calendarResource);
                            }
                        }

                        sqlConnection.Close();
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex.Message);
                        sqlConnection.Close();
                    }
                }
            }

            return calendarResourceList.ToArray();
        }

        public bool UpdateCalendarResource(int ID, CalendarResourceDto calendarResource)
        {
            throw new NotImplementedException();
        }
    }
}
