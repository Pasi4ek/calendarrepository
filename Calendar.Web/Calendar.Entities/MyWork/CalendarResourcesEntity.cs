﻿using System;
using System.Drawing;

namespace Calendar.Entities.MyWork
{
    /// <summary>
    /// Class CalendarResourcesEntity
    /// </summary>
    public class CalendarResourcesEntity
    {
        /// <summary>
        /// Gets or sets ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets ResourceID
        /// </summary>
        public int ResourceID { get; set; }

        /// <summary>
        /// Gets or sets ResourceName
        /// </summary>
        public string ResourceName { get; set; }

        /// <summary>
        /// Gets or sets Color
        /// </summary>
        public int Color { get; set; }

        /// <summary>
        /// Gets or sets Image
        /// </summary>
        public Image Image { get; set; }

        /// <summary>
        /// Gets or sets CustomField1
        /// </summary>
        public string CustomField1 { get; set; }

        /// <summary>
        /// Gets or sets ModifiedUTCTimeStamp
        /// </summary>
        public DateTime ModifiedUTCTimeStamp { get; set; }

        /// <summary>
        /// Gets or sets MaxDateTime2
        /// </summary>
        public DateTime MaxDateTime2 { get; set; }

        /// <summary>
        /// Gets or sets IsDeleted
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets EmployeeID
        /// </summary>
        public long EmployeeID { get; set; }
    }
}
