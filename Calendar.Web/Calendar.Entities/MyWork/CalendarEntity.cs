﻿using System;

namespace Calendar.Entities.MyWork
{
    /// <summary>
    /// Class CelendarEntity
    /// </summary>
    public class CalendarEntity
    {
        /// <summary>
        /// Gets or sets Caption
        /// </summary>
        public string Caption { get; set; }

        /// <summary>
        /// Gets or sets Start
        /// </summary>
        public DateTime Start { get; set; }

        /// <summary>
        /// Gets or sets Message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets Location
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Gets or sets Fuel
        /// </summary>
        public double Fuel { get; set; }

        /// <summary>
        /// Gets or sets Distance
        /// </summary>
        public double Distance { get; set; }

        /// <summary>
        /// Gets or sets Client
        /// </summary>
        public string Client { get; set; }

        /// <summary>
        /// Gets or sets Quality
        /// </summary>
        public double Quality { get; set; }

        /// <summary>
        /// Gets or sets Approved
        /// </summary>
        public string Approved { get; set; }

        /// <summary>
        /// Gets or sets Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets Longitube
        /// </summary>
        public string Longitube { get; set; }

        /// <summary>
        /// Gets or sets Latitude
        /// </summary>
        public string Latitude { get; set; }

        /// <summary>
        /// Gets or sets EbentDuraction
        /// </summary>
        public double EventDuraction { get; set; }

        /// <summary>
        /// Gets or sets Finish
        /// </summary>
        public DateTime Finish { get; set; }

        /// <summary>
        /// Gets or sets EventType
        /// </summary>
        public int EventType { get; set; }

        /// <summary>
        /// Gets or sets Options
        /// </summary>
        public int Options { get; set; }

        /// <summary>
        /// Gets or sets ResourceID 
        /// </summary>
        public int ResourceID { get; set; }

        /// <summary>
        /// Gets or sets ID
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// Gets or sets LabelColor
        /// </summary>
        public int LabelColor { get; set; }

        /// <summary>
        /// Gets or sets State
        /// </summary>
        public int State { get; set; }

        /// <summary>
        /// Gets or sets TaskID
        /// </summary>
        public int TaskID { get; set; }

        /// <summary>
        /// Gets or sets ResultMessage
        /// </summary>
        public string ResultMessage { get; set; }

        /// <summary>
        /// Gets or sets ClientID
        /// </summary>
        public long ClientID { get; set; }

        /// <summary>
        /// Gets or sets ModifiedUTCTimeStamp
        /// </summary>
        public DateTime ModifiedUTCTimeStamp { get; set; }

        /// <summary>
        /// Gets or sets MaxDiteTime
        /// </summary>
        public DateTime MaxDiteTime2 { get; set; }

        /// <summary>
        /// Gets or sets IsDeleted
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets ManualChecked
        /// </summary>
        public bool ManualChecked { get; set; }

        /// <summary>
        /// Gets or sets ParentID
        /// </summary>
        public int ParentID { get; set; }

        /// <summary>
        /// Gets or sets RecurrenceIndex
        /// </summary>
        public int RecurrenceIndex { get; set; }

        /// <summary>
        /// Gets or sest RecurrenceInfo
        /// </summary>
        public byte[] RecurrenceInfo { get; set; }
    }
}
