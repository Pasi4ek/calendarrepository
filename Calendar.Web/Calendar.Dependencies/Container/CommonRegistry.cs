﻿using Calendar.Core.Container;
using Calendar.Data.Container;
using StructureMap.Configuration.DSL;

namespace Calendar.Dependencies.Container
{
    public class CommonRegistry : Registry
    {
        public CommonRegistry()
        {
            Scan(scan =>
            {
                scan.Assembly(typeof(CoreRegistry).Assembly);
                scan.Assembly(typeof(DataRegistry).Assembly);
                scan.WithDefaultConventions();
            });
        }
    }
}
